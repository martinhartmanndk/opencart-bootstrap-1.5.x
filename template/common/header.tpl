<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <head>
        <meta charset="UTF-8" />
        <title><?php echo $title; ?></title>
        <base href="<?php echo $base; ?>" />
        <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; ?>" />
        <?php } ?>
        <?php if ($keywords) { ?>
        <meta name="keywords" content="<?php echo $keywords; ?>" />
        <?php } ?>
        <?php if ($icon) { ?>
        <link href="<?php echo $icon; ?>" rel="icon" />
        <?php } ?>
        <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
        <?php } ?>
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/ocb/stylesheet/stylesheet.css" />
        <?php foreach ($styles as $style) { ?>
        <!-- udkommenteret da de ikke er nødvendige -->
        <!-- link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" / -->
        <?php } ?>
        <script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
        <!--script type="text/javascript" src="catalog/view/javascript/jquery/jquery-2.0.js"></script-->
        <script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
        <link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
        <script type="text/javascript" src="catalog/view/javascript/common.js"></script>
        <?php foreach ($scripts as $script) { ?>
        <script type="text/javascript" src="<?php echo $script; ?>"></script>
        <?php } ?>
        <!--[if IE 7]> 
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/ocb/stylesheet/ie7.css" />
        <![endif]-->
        <!--[if lt IE 7]>
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/ocb/stylesheet/ie6.css" />
        <script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
        <script type="text/javascript">
        DD_belatedPNG.fix('#logo img');
        </script>
        <![endif]-->
        <?php if ($stores) { ?>
        <script type="text/javascript"><!--
        $(document).ready(function() {
            < ?php foreach ($stores as $store) { ? >
                    $('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
                    < ?php } ? >
            });
                    //--></script>
        <?php } ?>
        <?php echo $google_analytics; ?>
    </head>
    <body>
        <div class="container">
            <header id="header">
                <div class="row">
                    <div class="col-md-4">
                        <!-- logo -->
                        <?php if ($logo) { ?>
                        <a href="<?php echo $home; ?>"><img class="img-responsive" src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
                    </div><!-- end logo -->
                    <?php } ?>
                    <div class="col-md-2">
                        <?php echo $language; ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $currency; ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $cart; ?>
                    </div>                        
                    <div class="col-md-2">                                                                                             
                        <input class="form-control"type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />                                    
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="links"><a href="<?php echo $home; ?>"><?php echo $text_home; ?></a><a href="<?php echo $wishlist; ?>" id="wishlist-total"><?php echo $text_wishlist; ?></a><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a><a href="<?php echo $shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a><a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></div>
                    </div>
                </div>    
                    <div class="row">
                        <div class="col-md-12">
                            <div id="welcome">
                                <?php if (!$logged) { ?>
                                <?php echo $text_welcome; ?>
                                <?php } else { ?>
                                <?php echo $text_logged; ?>
                                <?php } ?>
                            </div>
                        </div>  
                    </div>  
                </div>
        </div>
    </div>
</header>

<div class="container">
    <?php if ($categories) { ?>
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <?php foreach ($categories as $category) { ?>
                    <?php if ($category['children']) { ?>
                    <li class="dropdown">
                        <a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $category['name']; ?> <span class="caret"></span></a>
                        <?php for ($i = 0; $i < count($category['children']);) { ?>
                        <ul class="dropdown-menu" role="menu">
                            <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
                            <?php for (; $i < $j; $i++) { ?>
                            <?php if (isset($category['children'][$i])) { ?>
                            <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
                            <?php } ?>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                        <?php } else { ?>
                    </li>
                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                    <?php } } ?>
                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <?php } ?>
    <?php if ($error) { ?>

    <div class="warning"><?php echo $error ?><img src="catalog/view/theme/ocb/image/close.png" alt="" class="close" /></div>

    <?php } ?>  
    <div id="notification"></div>














    
    