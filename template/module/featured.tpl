<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $heading_title; ?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <?php foreach ($products as $product) { ?>
            <div class="col-sm-3">
                <div class="thumbnail">
                    <?php if ($product['thumb']) { ?>
                    <a href="<?php echo $product['href']; ?>"><img class="img-responsive" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
                    <div class="caption">
                        <h3><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>
                        <div class="price">
                            <?php if (!$product['special']) { ?>
                            <?php echo $product['price']; ?>
                            <?php } else { ?>
                            <strike><?php echo $product['price']; ?></strike> <?php echo $product['special']; ?>
                            <?php } ?>
                        </div>
                        <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="btn btn-success btn-cart" />
                        <?php if ($product['rating']) { ?>
                        <div class="text-center product-rate"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>