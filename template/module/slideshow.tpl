<div id="myCarousel" class="carousel slide" data-ride="carousel">
<!-- Indicators -->
<ol class="carousel-indicators">
<?php
$i = 0;
foreach ($banners as $banner) {
    echo "<li data-target=\"#myCarousel\" data-slide-to=\"".$i."\" class=\"indicator\"></li>\n";
    $i++;
}
?>
</ol>
<!-- slides -->
<div class="carousel-inner">
<?php foreach ($banners as $banner) { ?>
<?php if ($banner['link']) { ?>
<div class="item"><a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></a></div>
<?php } else { ?>
<div class="item"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></div>
<?php } ?>
<?php } ?>
</div>
<!-- Controls -->
<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
</a>
<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
</a>
</div>

<script type="text/javascript">
//<![CDATA[ 
    jQuery(document).ready(function($){
    $('#myCarousel ol.carousel-indicators li:first').addClass('active');
       $('#myCarousel .item:first').addClass('active');
        $('#myCarousel').carousel({
        interval: 8000
    });
});
//]]> 
</script>