<!-- hent og brug bootstrap (KUN hvis du ikke allerede har loaded bootstrap 3)
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script-->

<script type="text/javascript" src="catalog/view/javascript/jcarousel.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jcarousel-responsive.js"></script>

<!-- Bruger her vores jcarousel til at præsentere produkter i en carousel -->

<div id="carousel<?php echo $module; ?>">
    <h3><?php echo $heading_title; ?></h3>
    <div class="jcarousel-wrapper">
        <div class="jcarousel text-center">
            <ul>
                <?php foreach ($products as $product) { ?>
                <?php if ($product['thumb']) { ?>
                <li class="thumbnail">
                    <a href="<?php echo $product['href']; ?>"><img class="img-responsive img-thumbnail" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
                    <h3><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>
                    <div class="price">
                        <span style="font-weight: bold;"><?php if (!$product['special']) { ?></span>
                        <span style="font-weight: bold;"><?php echo $product['price']; ?></span>
                        <?php } else { ?>
                        <strike style="color: red;" ><?php echo $product['price']; ?></strike> <span style="font-weight: bold;"><?php echo $product['special']; ?></span>
                        <?php } ?>
                        <br />
                        <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="btn btn-success btn-cart" />
                    </div>
                </li>
                <?php } ?>
                <?php } ?>
            </ul>
        </div>

        <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
        <a href="#" class="jcarousel-control-next">&rsaquo;</a>
        <!-- deaktiveret dots i bunden, kan aktiveres efter behov -->
        <!-- p class="jcarousel-pagination"></p -->
    </div>

</div>
