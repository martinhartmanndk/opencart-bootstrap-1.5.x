<script type="text/javascript" src="catalog/view/javascript/jcarousel.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jcarousel-responsive.js"></script>

<div id="carousel<?php echo $module; ?>">
    <div class="jcarousel-wrapper">
        <div class="jcarousel">
            <ul>
                <?php foreach ($banners as $banner) { ?>
                <li><a href="<?php echo $banner['link']; ?>"><img class="img-responsive" src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" /></a></li>
                <?php } ?>
            </ul>
        </div>

        <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
        <a href="#" class="jcarousel-control-next">&rsaquo;</a>
        <!-- deaktiveret dots i bunden, kan aktiveres efter behov -->
        <!-- p class="jcarousel-pagination"></p -->
    </div>

</div>
